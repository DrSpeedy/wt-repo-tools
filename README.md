## Wiltech Repo Tools

#### Zshrc/Bashrc
```sh
# Must be added to work, or executed before using this toolset
# You must import both public and private keys
export PUBKEY_ID="<GPG KEY ID>"
```

#### Usage:
```
wt <sub command> [args...]

b|build     Build project (Make wrapper)
c|clean     Clean project directory of build files and packages (Make wrapper)
d|deploy    Update the configured APT repository
    -u=|--user=         SSH user
    -p=|--port=         SSH port
    -h=|--host=         SSH host
    -d=|--remote-dir=   Remote directory of the APT repository
-h|--help   Show usage
```
