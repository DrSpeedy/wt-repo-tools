NAME=wt-repo-tools
VERSION=2.4

PKG_NAME=$(NAME)
PKG_ARCH=all

SYSCONF=/etc
PREFIX?=/usr/share
DOC_DIR=$(PREFIX)/doc/$(PKG_NAME)

BLD_DIR=$(PKG_NAME)_$(VERSION)_$(PKG_ARCH)
PKG=$(BLD_DIR).deb

GPG_PUBKEY_ID=${PUBKEY_ID}
SIGNING_NAME=wiltech

pkg: build
	sed -i 's/Package:.*/Package: $(PKG_NAME)/' $(BLD_DIR)/DEBIAN/control
	sed -i 's/Version:.*/Version: $(VERSION)/' $(BLD_DIR)/DEBIAN/control
	sed -i 's/Architecture:.*/Architecture: $(PKG_ARCH)/' $(BLD_DIR)/DEBIAN/control
	dpkg --build $(BLD_DIR)
	dpkg-sig -k $(GPG_PUBKEY_ID) --sign $(SIGNING_NAME) $(PKG)
	dpkg-sig -c $(PKG)

build:
	mkdir -p $(BLD_DIR)/usr/bin
	mkdir -p $(BLD_DIR)/usr/share/$(PKG_NAME)
	cp src/wt $(BLD_DIR)/usr/bin
	chmod +x $(BLD_DIR)/usr/bin/wt
	mkdir -p $(BLD_DIR)/$(DOC_DIR)
	cp LICENSE $(BLD_DIR)/$(DOC_DIR)
	cp -r DEBIAN $(BLD_DIR)

clean:
	rm -rf $(PKG) $(BLD_DIR) $(GIT_DIR)

all: pkg

tag:
	git tag $(VERSION)
	git push --tags

release: pkg tag

install:
	apt install ./$(PKG)

uninstall:
	apt remove --purge $(PKG_NAME)

.PHONY: build clean test tag release install uninstall all
